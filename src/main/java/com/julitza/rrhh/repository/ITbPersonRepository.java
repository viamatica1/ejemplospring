package com.julitza.rrhh.repository;


import com.julitza.rrhh.model.Tbperson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Repositorios -> Interfaces
// Heredan (se usa un extends) de JPARepository
// Significa que el proceso CRUD se ha ha definido
@Repository
public interface ITbPersonRepository extends JpaRepository<Tbperson, Long> {
}
