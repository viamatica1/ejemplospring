package com.julitza.rrhh.service;

//import com.julitza.rrhh.repository.ITbPersonRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.julitza.rrhh.model.Tbperson;
import com.julitza.rrhh.model.dto.DtoUsuario;
import com.julitza.rrhh.model.dto.GenericResponse;
import com.julitza.rrhh.repository.ITbPersonRepository;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class TbPersonService {
    @Autowired
    ITbPersonRepository iTbPersonRepository;

    @Value(value = "${jeyson.seguridad.clave}")
    String clave;

    Gson gson = new Gson().newBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();

    public String getAllPerson(String dataInput)  {
        // EXTRAER JSONSCHEMA
        InputStream schemaPerson = TbPersonService.class.getClassLoader().getResourceAsStream("templates/jsonschemaexample.schema.json");
        //DEFINIR EL OBJETO EN BASE AL JSONCHEMA
        JsonSchema schema = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4).getSchema(schemaPerson);
        List<Tbperson> person =  iTbPersonRepository.findAll();
        String datosPersona = "";
        ObjectMapper om = new ObjectMapper();
        String response = "";
        try{
            for(int i = 0; i < person.size(); i++){
                DtoUsuario dto = new DtoUsuario("Peter", "Male");
                datosPersona = dto.datosPersona(person.get(i));
            }
             response = gson.toJson(GenericResponse
                    .builder()
                    .codRespuesta(200)
                    .bodyResponse(gson.fromJson(datosPersona, Map.class))
                    .msjUsuario("EJERCICIO PRUEBA")
                    .build());
            Tbperson tbperson = gson.fromJson("{ aaaaa", Tbperson.class);
            //MAPEAMOS EL JSON DE ENTRADA/SALIDA EN UN OBJETO JSON
            JsonNode dataInJson = om.readTree(dataInput);
            // OBTENEMOS LOS ERRORES DE SCHEMA
            Set<ValidationMessage> errores = schema.validate(dataInJson);
            // RECORREMOS LOS ERRORES
            for (ValidationMessage mensaje: errores){
                log.warn("Error en: "+ mensaje.getMessage());
            }
            }catch(JsonParseException e ){
                log.error("La estructura no se puede mapear: {}", e.getLocalizedMessage());
            }catch (Exception e){
                log.error("Error: "+ e.getLocalizedMessage());
            }

        return response;
    }

}
