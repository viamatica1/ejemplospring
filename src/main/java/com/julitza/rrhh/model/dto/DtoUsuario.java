package com.julitza.rrhh.model.dto;


import com.google.gson.Gson;
import com.julitza.rrhh.model.Tbcharge;
import com.julitza.rrhh.model.Tbperson;
import com.julitza.rrhh.service.TbChargeService;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class DtoUsuario {



    private final String name;
    private final String genre;
    Gson gson = new Gson()
            .newBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .serializeNulls()
            .create();

    public String datosPersona(Tbperson person){
        Map<String, Object> datosper = new HashMap<>();
        datosper.put("NOMBRE", name);
        datosper.put("genero", genre);
        datosper.put("fnacimiento", person.getBirthdate().toString());
        return gson.toJson(datosper);
    }

}
