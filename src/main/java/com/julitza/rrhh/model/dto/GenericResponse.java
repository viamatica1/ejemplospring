package com.julitza.rrhh.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Map;


@Data
@AllArgsConstructor
@Builder
public class GenericResponse {

    String msjUsuario;
    Integer codRespuesta;
    Map<String, Object> bodyResponse;
}
