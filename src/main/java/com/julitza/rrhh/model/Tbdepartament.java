package com.julitza.rrhh.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Tbdepartament {
    private int iddepartament;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "iddepartament", nullable = false)
    public int getIddepartament() {
        return iddepartament;
    }

    public void setIddepartament(int iddepartament) {
        this.iddepartament = iddepartament;
    }

    private String namedepartament;

    @Basic
    @Column(name = "namedepartament", nullable = true, length = 50)
    public String getNamedepartament() {
        return namedepartament;
    }

    public void setNamedepartament(String namedepartament) {
        this.namedepartament = namedepartament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbdepartament that = (Tbdepartament) o;
        return iddepartament == that.iddepartament && Objects.equals(namedepartament, that.namedepartament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iddepartament, namedepartament);
    }
}
