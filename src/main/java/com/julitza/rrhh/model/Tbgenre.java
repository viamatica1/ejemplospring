package com.julitza.rrhh.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Tbgenre {
    private int idgenre;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "idgenre", nullable = false)
    public int getIdgenre() {
        return idgenre;
    }

    public void setIdgenre(int idgenre) {
        this.idgenre = idgenre;
    }

    private String genre;

    @Basic
    @Column(name = "genre", nullable = false, length = 2)
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbgenre tbgenre = (Tbgenre) o;
        return idgenre == tbgenre.idgenre && Objects.equals(genre, tbgenre.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idgenre, genre);
    }
}
