package com.julitza.rrhh.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Objects;

@Entity
public class Tbpersonchrage {
    private long idperson;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "idperson", nullable = false)
    public long getIdperson() {
        return idperson;
    }

    public void setIdperson(long idperson) {
        this.idperson = idperson;
    }

    private long idcharge;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "idcharge", nullable = false)
    public long getIdcharge() {
        return idcharge;
    }

    public void setIdcharge(long idcharge) {
        this.idcharge = idcharge;
    }

    private long datechargeperson;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "datechargeperson", nullable = false)
    public long getDatechargeperson() {
        return datechargeperson;
    }

    public void setDatechargeperson(long datechargeperson) {
        this.datechargeperson = datechargeperson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbpersonchrage that = (Tbpersonchrage) o;
        return idperson == that.idperson && idcharge == that.idcharge && datechargeperson == that.datechargeperson;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idperson, idcharge, datechargeperson);
    }
}
