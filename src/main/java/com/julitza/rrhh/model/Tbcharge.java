package com.julitza.rrhh.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Tbcharge {
    private long idcharge;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "idcharge", nullable = false)
    public long getIdcharge() {
        return idcharge;
    }

    public void setIdcharge(long idcharge) {
        this.idcharge = idcharge;
    }

    private String namecharge;

    @Basic
    @Column(name = "namecharge", nullable = false, length = -1)
    public String getNamecharge() {
        return namecharge;
    }

    public void setNamecharge(String namecharge) {
        this.namecharge = namecharge;
    }

    private int iddepartament;

    @Basic
    @Column(name = "iddepartament", nullable = false)
    public int getIddepartament() {
        return iddepartament;
    }

    public void setIddepartament(int iddepartament) {
        this.iddepartament = iddepartament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbcharge tbcharge = (Tbcharge) o;
        return idcharge == tbcharge.idcharge && iddepartament == tbcharge.iddepartament && Objects.equals(namecharge, tbcharge.namecharge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcharge, namecharge, iddepartament);
    }
}
