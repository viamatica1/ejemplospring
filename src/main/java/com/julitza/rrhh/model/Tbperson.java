package com.julitza.rrhh.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import java.sql.Date;
import java.util.Objects;

@Entity
public class Tbperson {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idperson", nullable = false)
    private long idperson;


    @Basic
    @Column(name = "identification", nullable = false, length = 13)
    @NotNull
    @NotBlank
    @Pattern(regexp = "[\s]")
    private String identification;


    private String firstname;

    @Basic
    @Column(name = "firstname", nullable = false, length = 50)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    private String lastname;

    @Basic
    @Column(name = "lastname", nullable = false, length = 50)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    private Date birthdate;

    @Basic
    @Column(name = "birthdate", nullable = true)
    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    private int idgenre;

    @Basic
    @Column(name = "idgenre", nullable = false)
    public int getIdgenre() {
        return idgenre;
    }

    public void setIdgenre(int idgenre) {
        this.idgenre = idgenre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbperson tbperson = (Tbperson) o;
        return idperson == tbperson.idperson && idgenre == tbperson.idgenre && Objects.equals(identification, tbperson.identification) && Objects.equals(firstname, tbperson.firstname) && Objects.equals(lastname, tbperson.lastname) && Objects.equals(birthdate, tbperson.birthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idperson, identification, firstname, lastname, birthdate, idgenre);
    }
}
