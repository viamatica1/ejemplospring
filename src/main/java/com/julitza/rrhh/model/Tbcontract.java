package com.julitza.rrhh.model;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
public class Tbcontract {
    private long idcontract;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "idcontract", nullable = false)
    public long getIdcontract() {
        return idcontract;
    }

    public void setIdcontract(long idcontract) {
        this.idcontract = idcontract;
    }

    private Date datecontract;

    @Basic
    @Column(name = "datecontract", nullable = false)
    public Date getDatecontract() {
        return datecontract;
    }

    public void setDatecontract(Date datecontract) {
        this.datecontract = datecontract;
    }

    private long idperson;

    @Basic
    @Column(name = "idperson", nullable = false)
    public long getIdperson() {
        return idperson;
    }

    public void setIdperson(long idperson) {
        this.idperson = idperson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tbcontract that = (Tbcontract) o;
        return idcontract == that.idcontract && idperson == that.idperson && Objects.equals(datecontract, that.datecontract);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcontract, datecontract, idperson);
    }
}
